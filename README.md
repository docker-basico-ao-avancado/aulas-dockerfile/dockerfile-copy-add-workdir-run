# Dockerfile - COPY, ADD, WORKDIR e RUN

## Contexto

Este repositório foi criado como conteúdo adicional para a aula [**Dockerfile - COPY, ADD, WORKDIR e RUN**](https://www.udemy.com/course/docker-do-basico-ao-avancado/learn/lecture/34071422#overview) do curso [**Docker - Básico ao Avançado**](https://www.udemy.com/course/docker-do-basico-ao-avancado/?couponCode=DOCKER_DEZ24), por Daniel Gil.

## Documentação Usada na Aula

- [Dockerfile - COPY](https://docs.docker.com/engine/reference/builder/#copy)
- [Dockerfile - ADD](https://docs.docker.com/engine/reference/builder/#add)
- [Dockerfile - WORKDIR](https://docs.docker.com/engine/reference/builder/#workdir)
- [Dockerfile - RUN](https://docs.docker.com/engine/reference/builder/#run)

## Conheça os Cursos

> Quer conhecer um dos cursos?
>
> Aqui você encontra links com cupons de desconto para:

**Docker - Básico ao Avançado**

[![Docker - Básico ao Avançado](https://danielgilcursos.blob.core.windows.net/images/docker-basico-ao-avancado.png)](https://www.udemy.com/course/docker-do-basico-ao-avancado/?couponCode=DOCKER_DEZ24)

**Terraform - Básico ao Avançado**

[![Terraform - Básico ao Avançado](https://danielgilcursos.blob.core.windows.net/images/terraform-basico-ao-avancado.png)](https://www.udemy.com/course/terraform-do-basico-ao-avancado/?couponCode=TERRAFORM_DEZ24)
